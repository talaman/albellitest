﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AlbelliTest;
using AlbelliTest.Controllers;
using System.Web.Http.Results;
using AlbelliTest.Models;

namespace AlbelliTest.Tests.Controllers
{
    [TestClass]
    public class CustomersTest
    {


        [TestMethod]
        public void Create()
        {
            CustomersController controller = new CustomersController();
            Customer demoCustomer = GetDemoCustomer();
            var result =
              controller.PostCustomer(demoCustomer) as CreatedAtRouteNegotiatedContentResult<Customer>;

            Assert.IsNotNull(result);
            Assert.AreEqual(result.RouteName, "DefaultApi");
            Assert.AreEqual(result.RouteValues["id"], result.Content.Customer_ID);
            Assert.AreEqual(result.Content.Name, demoCustomer.Name);
            
        }



        [TestMethod]
        public void Get()
        {
            CustomersController controller = new CustomersController();
            Customer demoCustomer = GetDemoCustomer();// (controller.PostCustomer(GetDemoCustomer()) as CreatedAtRouteNegotiatedContentResult<Customer>).Content;

            IEnumerable<CustomerResponse> result = controller.GetCustomers();

            Assert.IsNotNull(result);
            Assert.AreEqual(demoCustomer.Name, result.Single(x => x.Customer_ID == demoCustomer.Customer_ID).Name);
        }

        [TestMethod]
        public void GetById()
        {
            CustomersController controller = new CustomersController();
            Customer demoCustomer = GetDemoCustomer(); //(controller.PostCustomer(GetDemoCustomer()) as CreatedAtRouteNegotiatedContentResult<Customer>).Content;

            IHttpActionResult result = controller.GetCustomer(demoCustomer.Customer_ID);
            var contentResult = result as OkNegotiatedContentResult<Customer>;

            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Customer c = contentResult.Content;
            
            Assert.AreEqual(demoCustomer.Name, c.Name);
        }


        [TestMethod]
        public void GetReturnsNotFound()
        {
            // Arrange
            var controller = new CustomersController();

            // Act
            IHttpActionResult actionResult = controller.GetCustomer(Guid.NewGuid());

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }

        [TestMethod]
        public void CreateOrder()
        {
            CustomersController controller = new CustomersController();
            Customer demoCustomer = GetDemoCustomer();//(controller.PostCustomer(GetDemoCustomer()) as CreatedAtRouteNegotiatedContentResult<Customer>).Content;
            Order demoOrder = GetDemoOrder();
            demoOrder.Customer_Id = demoCustomer.Customer_ID;
            var result =
              controller.CreateOrder(demoOrder) as CreatedAtRouteNegotiatedContentResult<Order>;

            Assert.IsNotNull(result);
            Assert.AreEqual(result.RouteName, "CreateOrder");
            Assert.AreEqual(result.RouteValues["id"], result.Content.Order_ID);
            Assert.AreEqual(result.Content.CreatedDate, demoOrder.CreatedDate);

        }

        [TestMethod]
        public void zCleanUp()
        {
            Guid cid = GetDemoCustomer().Customer_ID;
            var db = new AlbelliTestDBEntities();
            Customer customer = db.Customers.Single(c => c.Customer_ID == cid);
            var orders = customer.Orders;
            db.Orders.RemoveRange(orders);
            db.Customers.Remove(customer);
            db.SaveChanges();
            db.Dispose();
        }


        Customer GetDemoCustomer()
        {
            return new Customer() { Customer_ID=Guid.Parse("38402EE9-0E81-4EF6-85B4-84B3DD86B1B3"), Name = "Daniel", Email = "daniel.talaman@gmail.com" };
        }

        Order GetDemoOrder() {
            return new Order() { Price = 12323, CreatedDate = DateTime.Now };
        }

    }
}
