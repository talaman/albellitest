﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AlbelliTest.Models;

namespace AlbelliTest.Controllers
{
    public class CustomersController : ApiController
    {
        private AlbelliTestDBEntities db = new AlbelliTestDBEntities();

        // GET: Customers
        public IEnumerable<CustomerResponse> GetCustomers()
        {
            return (from x in db.Customers select new CustomerResponse { Customer_ID = x.Customer_ID, Name = x.Name, Email = x.Email }).ToList();
        }

        // GET: Customers/5
        [ResponseType(typeof(Customer))]
        public IHttpActionResult GetCustomer(Guid id)
        {
            Customer customer = db.Customers.Find(id);
            if (customer == null)
            {
                return NotFound();
            }
            return Ok(customer);
        }

        // PUT: Customers/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCustomer(Guid id, Customer customer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != customer.Customer_ID)
            {
                return BadRequest();
            }

            db.Entry(customer).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CustomerExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: Customers
        [ResponseType(typeof(Customer))]
        public IHttpActionResult PostCustomer(Customer customer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (customer.Customer_ID == null)
            {
                customer.Customer_ID = Guid.NewGuid();
            }
            db.Customers.Add(customer);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (CustomerExists(customer.Customer_ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = customer.Customer_ID }, customer);
        }

        [Route("V1/Customers/{id}/Orders",Name = "CreateOrder")]
        [ResponseType(typeof(Order))]
        public IHttpActionResult CreateOrder(Order order)
        {
            if (!ModelState.IsValid || order.Customer_Id == null)
            {
                return BadRequest(ModelState);
            }
            db.Orders.Add(order);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (OrderExists(order.Order_ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("CreateOrder", new { id = order.Order_ID }, order);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CustomerExists(Guid id)
        {
            return db.Customers.Count(e => e.Customer_ID == id) > 0;
        }
        private bool OrderExists(int id)
        {
            return db.Orders.Count(e => e.Order_ID == id) > 0;
        }
    }
}