﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AlbelliTest.Models
{
    public class CustomerResponse
    {
        public System.Guid Customer_ID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }
}